package com.task7405.restapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task7405.restapi.model.Office;
import com.task7405.restapi.repository.OfficeRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class OfficeController {

    @Autowired
    OfficeRepository officeRepository;

    @PostMapping("/office/create")
    public ResponseEntity<Object> createemployee(@RequestBody Office office) {
        try {
            Office newRole = new Office();
            newRole.setCity(office.getCity());
            newRole.setAddressLine(office.getAddressLine());
            newRole.setCountry(office.getCountry());
            newRole.setPhone(office.getPhone());
            newRole.setState(office.getState());
            newRole.setTeritory(office.getTeritory());

            Office savedRole = officeRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified office: " + e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/office/update/{id}")
    public ResponseEntity<Object> updateOffice(@PathVariable("id") int id, @RequestBody Office office) {
        try {
            Optional<Office> officeData = officeRepository.findOfficeById(id);
            if (officeData.isPresent()) {
                Office newRole = officeData.get();
                newRole.setCity(office.getCity());
                newRole.setAddressLine(office.getAddressLine());
                newRole.setCountry(office.getCountry());
                newRole.setPhone(office.getPhone());
                newRole.setState(office.getState());
                newRole.setTeritory(office.getTeritory());

                Office savedRole = officeRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified office: " + id + "  for update.");
        }

    }

    @GetMapping("/office/details/{id}")
    public ResponseEntity<Office> getOfficeById(@PathVariable int id) {
        try {
            Optional<Office> OfficeData = officeRepository.findOfficeById(id);
            if (OfficeData.isPresent()) {
                return new ResponseEntity<>(OfficeData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/office/all")
    public ResponseEntity<List<Office>> getAllOffice() {
        try {
            List<Office> listOffice = new ArrayList<Office>();
            officeRepository.findAll().forEach(listOffice::add);

            return new ResponseEntity<>(listOffice, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/office/delete/{id}")
    public ResponseEntity<Object> deleteOfficeById(@PathVariable int id) {
        try {
            Optional<Office> OfficeData = officeRepository.findOfficeById(id);
            Office OfficeDelete = OfficeData.get();
            officeRepository.delete(OfficeDelete);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
