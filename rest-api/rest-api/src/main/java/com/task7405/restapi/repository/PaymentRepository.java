package com.task7405.restapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.task7405.restapi.model.Payment;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    @Query(value = "SELECT * FROM payments WHERE id =:id", nativeQuery = true)
    Optional<Payment> findPaymentById(@Param("id") int id);

    @Query(value = " SELECT payments.* FROM payments INNER JOIN customers ON payments.customer_id = customers.id WHERE customers.id = :customerId", nativeQuery = true)
    List<Payment> findPaymentByCustomerId(@Param("customerId") int customerId);

}
