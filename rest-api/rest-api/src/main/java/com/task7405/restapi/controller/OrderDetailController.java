package com.task7405.restapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task7405.restapi.model.Order;
import com.task7405.restapi.model.Orderdetail;
import com.task7405.restapi.model.Product;
import com.task7405.restapi.repository.OrderDetailRepository;
import com.task7405.restapi.repository.OrderRepository;
import com.task7405.restapi.repository.ProductRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class OrderDetailController {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    OrderDetailRepository orderDetailRepository;

    @PostMapping("/orderdetail/create/{orderId}/{productId}")
    public ResponseEntity<Object> createOrderDetail(@PathVariable("orderId") int orderId,
            @PathVariable("productId") int productId,
            @RequestBody Orderdetail orderdetail) {
        try {
            Optional<Order> orderDetailData = orderRepository.findOrderById(orderId);
            Optional<Product> productData = productRepository.findProductById(productId);
            if (orderDetailData.isPresent() && productData.isPresent()) {
                Orderdetail newRole = new Orderdetail();
                newRole.setQuantityOrder(orderdetail.getQuantityOrder());
                newRole.setPriceEach(orderdetail.getPriceEach());

                Order _order = orderDetailData.get();
                newRole.setOrder(_order);
                Product _product = productData.get();
                newRole.setProduct(_product);

                Orderdetail savedRole = orderDetailRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified orderdetail: " +
                            e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/orderdetail/update/{id}")
    public ResponseEntity<Object> updateorderdetail(@PathVariable("id") int id, @RequestBody Orderdetail orderdetail) {
        try {
            Optional<Orderdetail> orderDetailData = orderDetailRepository.findOrderDetailById(id);
            if (orderDetailData.isPresent()) {
                Orderdetail newRole = orderDetailData.get();
                newRole.setQuantityOrder(orderdetail.getQuantityOrder());
                newRole.setPriceEach(orderdetail.getPriceEach());

                Orderdetail savedRole = orderDetailRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified orderdetail: " + id + "  for update.");
        }

    }

    @GetMapping("/orderdetail/all")
    public ResponseEntity<List<Orderdetail>> getAllOrderDetail() {
        try {
            List<Orderdetail> orderdetail = new ArrayList<Orderdetail>();
            orderDetailRepository.findAll().forEach(orderdetail::add);
            return new ResponseEntity<>(orderdetail, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orderdetail/details/{id}")
    public ResponseEntity<Orderdetail> getOrderDetailById(@PathVariable int id) {
        try {
            Optional<Orderdetail> orderDetailData = orderDetailRepository.findOrderDetailById(id);
            if (orderDetailData.isPresent()) {
                return new ResponseEntity<>(orderDetailData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/customer_product/{orderId}/{productId}/orders")
    public ResponseEntity<List<Orderdetail>> getOrderDetailByCustomerIdAndProductid(
            @PathVariable(value = "orderId") int orderId, @PathVariable(value = "productId") int productId) {
        try {
            List<Orderdetail> orderdetailData = orderDetailRepository
                    .findOrderDetailByOrderIdAndProductId(orderId, productId);
            if (!orderdetailData.isEmpty()) {
                return new ResponseEntity<>(orderdetailData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/order/{orderId}/order_detail")
    public ResponseEntity<List<Orderdetail>> getOrderDetailByOrderId(
            @PathVariable(value = "orderId") int orderId) {
        try {
            List<Orderdetail> orderdetailData = orderDetailRepository
                    .findOrderDetailByOrderId(orderId);
            if (!orderdetailData.isEmpty()) {
                return new ResponseEntity<>(orderdetailData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/orderdetail/delete/{id}")
    public ResponseEntity<Object> deleteOrderDeatilById(@PathVariable int id) {
        try {
            Optional<Orderdetail> orderdetailData = orderDetailRepository.findOrderDetailById(id);
            Orderdetail orderdetailDelete = orderdetailData.get();
            orderDetailRepository.delete(orderdetailDelete);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
