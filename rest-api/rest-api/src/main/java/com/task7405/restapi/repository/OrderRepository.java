package com.task7405.restapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.task7405.restapi.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query(value = "SELECT * FROM orders WHERE id =:id", nativeQuery = true)
    Optional<Order> findOrderById(@Param("id") int id);

    @Query(value = " SELECT orders.* FROM orders INNER JOIN customers ON orders.customer_id = customers.id WHERE customers.id=:customerId", nativeQuery = true)
    List<Order> findOrderByCustomerId(@Param("customerId") int customerId);

}
