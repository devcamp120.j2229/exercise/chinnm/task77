package com.task7405.restapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task7405.restapi.model.Customer;
import com.task7405.restapi.model.Order;
import com.task7405.restapi.repository.CustomerRepository;
import com.task7405.restapi.repository.OrderRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class OrderCOntroller {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    OrderRepository orderRepository;

    @PostMapping("/order/create/{id}")
    public ResponseEntity<Object> createorder(@PathVariable("id") int id,
            @RequestBody Order order) {
        try {
            Optional<Customer> customerData = customerRepository.findCustomerById(id);
            if (customerData.isPresent()) {
                Order newRole = new Order();
                newRole.setComments(order.getComments());
                newRole.setStatus(order.getStatus());
                newRole.setOrderDate(new Date());
                newRole.setRequiredDate(order.getRequiredDate());
                newRole.setShippedDate(order.getShippedDate());

                Customer _customer = customerData.get();
                newRole.setCustomers(_customer);

                Order savedRole = orderRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified customer: " +
                            e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/order/update/{id}")
    public ResponseEntity<Object> updateorder(@PathVariable("id") int id, @RequestBody Order order) {
        try {
            Optional<Order> orderData = orderRepository.findOrderById(id);
            if (orderData.isPresent()) {
                Order newRole = orderData.get();
                newRole.setComments(order.getComments());
                newRole.setStatus(order.getStatus());

                newRole.setRequiredDate(order.getRequiredDate());
                newRole.setShippedDate(order.getShippedDate());

                Order savedRole = orderRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified order: " + id + "  for update.");
        }

    }

    @GetMapping("/order/all")
    public ResponseEntity<List<Order>> getAllOrder() {
        try {
            List<Order> order = new ArrayList<Order>();
            orderRepository.findAll().forEach(order::add);
            return new ResponseEntity<>(order, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/order/details/{id}")
    public ResponseEntity<Order> getOrderById(@PathVariable int id) {
        try {
            Optional<Order> orderData = orderRepository.findOrderById(id);
            if (orderData.isPresent()) {
                return new ResponseEntity<>(orderData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/customer/{customerId}/orders")
    public ResponseEntity<List<Order>> getOrderByCustomerId(
            @PathVariable(value = "customerId") int customerId) {
        try {
            List<Order> orderData = orderRepository.findOrderByCustomerId(customerId);
            if (!orderData.isEmpty()) {
                return new ResponseEntity<>(orderData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/order/delete/{id}")
    public ResponseEntity<Object> deleteOrderById(@PathVariable int id) {
        try {
            Optional<Order> orderData = orderRepository.findOrderById(id);
            Order orderDelete = orderData.get();
            orderRepository.delete(orderDelete);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
