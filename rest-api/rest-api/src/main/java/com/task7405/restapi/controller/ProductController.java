package com.task7405.restapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task7405.restapi.model.Product;
import com.task7405.restapi.model.ProductLine;
import com.task7405.restapi.repository.ProductLineRepository;
import com.task7405.restapi.repository.ProductRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class ProductController {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductLineRepository productLineRepository;

    @GetMapping("/product/details/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable int id) {
        try {
            Optional<Product> productData = productRepository.findProductById(id);
            if (productData.isPresent()) {
                return new ResponseEntity<>(productData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/product/create/{id}")
    public ResponseEntity<Object> createProduct(@PathVariable("id") int id, @RequestBody Product product) {
        try {
            Optional<ProductLine> productLineData = productLineRepository.findProductLineById(id);
            if (productLineData.isPresent()) {
                Product newRole = new Product();
                newRole.setBuyPrice(product.getBuyPrice());
                newRole.setProductCode(product.getProductCode());
                newRole.setProductDescription(product.getProductDescription());
                newRole.setProductName(product.getProductName());
                newRole.setProductScale(product.getProductScale());
                newRole.setProductVendor(product.getProductVendor());
                newRole.setQuantityInStock(product.getQuantityInStock());

                ProductLine _productLine = productLineData.get();
                newRole.setProduct_line(_productLine);

                Product savedRole = productRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Product: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/product/update/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable("id") int id, @RequestBody Product product) {
        try {
            Optional<Product> productData = productRepository.findProductById(id);
            if (productData.isPresent()) {
                Product newRole = productData.get();
                newRole.setBuyPrice(product.getBuyPrice());
                newRole.setProductCode(product.getProductCode());
                newRole.setProductDescription(product.getProductDescription());
                newRole.setProductName(product.getProductName());
                newRole.setProductScale(product.getProductScale());
                newRole.setProductVendor(product.getProductVendor());
                newRole.setQuantityInStock(product.getQuantityInStock());
                Product savedRole = productRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified product: " + id + "  for update.");
        }

    }

    @GetMapping("/product/all")
    public ResponseEntity<List<Product>> getAllProduct() {
        try {
            List<Product> product = new ArrayList<Product>();
            productRepository.findAll().forEach(product::add);
            return new ResponseEntity<>(product, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/productline/{productLineId}/products")
    public ResponseEntity<List<Product>> getProductByProductLineId(
            @PathVariable(value = "productLineId") int productLineId) {
        try {
            List<Product> productData = productRepository.findProductByProductLineId(productLineId);
            if (!productData.isEmpty()) {
                return new ResponseEntity<>(productData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/product/delete/{id}")
    public ResponseEntity<Object> deleteProductById(@PathVariable int id) {
        try {
            Optional<Product> productData = productRepository.findProductById(id);
            Product productDelete = productData.get();
            productRepository.delete(productDelete);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
