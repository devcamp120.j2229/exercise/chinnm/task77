package com.task7405.restapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task7405.restapi.model.Customer;
import com.task7405.restapi.repository.CustomerRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class CustomerController {
    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/customer_detail_by_fristname/{firstname}")
    public ResponseEntity<Object> getUserById(@PathVariable(name = "firstname", required = true) String firstname) {
        List<Customer> userFounded = customerRepository.findCustomersByFirstNameLike(firstname);

        if (!userFounded.isEmpty()) {
            return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/customer_by_country_orderby_firstname/{country}/{numberPage}")
    // @GetMapping("/customer_by_country_orderby_firstname/{country}")
    public ResponseEntity<List<Customer>> getCustomerByCountryOrderByFirstname(
            @PathVariable("country") String country, @PathVariable("numberPage") int numberPage) {
        // @PathVariable("country") String country) {

        int length = 3;
        int start = numberPage - 1;
        try {

            List<Customer> listCustomer = customerRepository.findCustomersByCountryOrderByFirstName(country,
                    PageRequest.of(start, length));
            if (!listCustomer.isEmpty()) {
                return new ResponseEntity<>(listCustomer, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customer_by_city_and_state/{cỉty}/{state}/{numberPage}")
    public ResponseEntity<List<Customer>> getCustomerByCityAndState(
            @PathVariable("city") String city, @PathVariable("state") String state,
            @PathVariable("numberPage") int numberPage) {
        // @PathVariable("country") String country) {

        int length = 3;
        int start = numberPage - 1;
        try {

            List<Customer> listCustomer = customerRepository.findCustomersByCityAndState(city, state,
                    PageRequest.of(start, length));
            if (!listCustomer.isEmpty()) {
                return new ResponseEntity<>(listCustomer, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update_contry/{country}/{id}")

    public ResponseEntity<Object> updateCountry(@PathVariable("country") String country,
            @PathVariable("id") int id) {
        try {
            System.out.println("--------------------------------------");
            System.out.println("----------------LAM DAN DAY-------------------------");
            int countryUpdate = customerRepository.updateCountry(country, id);

            return new ResponseEntity<>(countryUpdate, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getMessage());
        }
    }

    @PostMapping("/customer/create")
    public ResponseEntity<Object> createcustomer(@RequestBody Customer customer) {
        try {
            Customer newRole = new Customer();
            newRole.setAddress(customer.getAddress());
            newRole.setCity(customer.getCity());
            newRole.setCountry(customer.getCountry());
            newRole.setCreditLimit(customer.getCreditLimit());
            newRole.setLastName(customer.getLastName());
            newRole.setFirstName(customer.getFirstName());
            newRole.setPhoneNumber(customer.getPhoneNumber());
            newRole.setPostalCode(customer.getPostalCode());
            newRole.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
            newRole.setState(customer.getState());

            Customer savedRole = customerRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/customer/update/{id}")
    public ResponseEntity<Object> updateCustomer(@PathVariable("id") int id, @RequestBody Customer customer) {
        try {
            Optional<Customer> customerData = customerRepository.findCustomerById(id);
            if (customerData.isPresent()) {
                Customer newRole = customerData.get();
                newRole.setAddress(customer.getAddress());
                newRole.setCity(customer.getCity());
                newRole.setCountry(customer.getCountry());
                newRole.setCreditLimit(customer.getCreditLimit());
                newRole.setFirstName(customer.getFirstName());
                newRole.setLastName(customer.getLastName());
                newRole.setPhoneNumber(customer.getPhoneNumber());
                newRole.setPostalCode(customer.getPostalCode());
                newRole.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
                newRole.setState(customer.getState());

                Customer savedcustomer = customerRepository.save(newRole);
                return new ResponseEntity<>(savedcustomer, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified customer: " + id + "  for update.");
        }

    }

    @GetMapping("/customer/details/{id}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable int id) {
        try {
            Optional<Customer> CustomerData = customerRepository.findCustomerById(id);
            if (CustomerData.isPresent()) {
                return new ResponseEntity<>(CustomerData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/customer/all")
    public ResponseEntity<List<Customer>> getAllCustomer() {
        try {
            List<Customer> listCustomer = new ArrayList<Customer>();
            customerRepository.findAll().forEach(listCustomer::add);

            return new ResponseEntity<>(listCustomer, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/customer/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable int id) {
        try {
            Optional<Customer> CustomerData = customerRepository.findCustomerById(id);
            Customer CustomerDelete = CustomerData.get();
            customerRepository.delete(CustomerDelete);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
