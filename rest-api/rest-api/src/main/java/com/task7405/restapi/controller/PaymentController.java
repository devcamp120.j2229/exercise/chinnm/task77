package com.task7405.restapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task7405.restapi.model.Customer;
import com.task7405.restapi.model.Payment;
import com.task7405.restapi.repository.CustomerRepository;
import com.task7405.restapi.repository.PaymentRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class PaymentController {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    PaymentRepository paymentRepository;

    @PostMapping("/payment/create/{id}")
    public ResponseEntity<Object> createPayment(@PathVariable("id") int id,
            @RequestBody Payment payment) {
        try {
            Optional<Customer> customerData = customerRepository.findCustomerById(id);
            if (customerData.isPresent()) {
                Payment newRole = new Payment();
                newRole.setAmount(payment.getAmount());
                newRole.setCheckNumber(payment.getCheckNumber());
                newRole.setPaymentDate(payment.getPaymentDate());

                Customer _customer = customerData.get();
                newRole.setCustomer(_customer);

                Payment savedRole = paymentRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified payment: " +
                            e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/payment/update/{id}")
    public ResponseEntity<Object> updatePayment(@PathVariable("id") int id, @RequestBody Payment payment) {
        try {
            Optional<Payment> paymentData = paymentRepository.findPaymentById(id);
            if (paymentData.isPresent()) {
                Payment newRole = paymentData.get();
                newRole.setAmount(payment.getAmount());
                newRole.setCheckNumber(payment.getCheckNumber());
                newRole.setPaymentDate(payment.getPaymentDate());

                Payment savedRole = paymentRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified payment: " + id + "  for update.");
        }

    }

    @GetMapping("/payment/all")
    public ResponseEntity<List<Payment>> getAllPayment() {
        try {
            List<Payment> payment = new ArrayList<Payment>();
            paymentRepository.findAll().forEach(payment::add);
            return new ResponseEntity<>(payment, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/payment/details/{id}")
    public ResponseEntity<Payment> getPaymentById(@PathVariable int id) {
        try {
            Optional<Payment> paymentData = paymentRepository.findPaymentById(id);
            if (paymentData.isPresent()) {
                return new ResponseEntity<>(paymentData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/customer/{customerId}/payments")
    public ResponseEntity<List<Payment>> getPaymentByCustomerId(
            @PathVariable(value = "customerId") int customerId) {
        try {
            List<Payment> paymentData = paymentRepository.findPaymentByCustomerId(customerId);
            if (!paymentData.isEmpty()) {
                return new ResponseEntity<>(paymentData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/payment/delete/{id}")
    public ResponseEntity<Object> deletePaymentById(@PathVariable int id) {
        try {
            Optional<Payment> orderDelete = paymentRepository.findPaymentById(id);
            Payment paymentDelete = orderDelete.get();
            paymentRepository.delete(paymentDelete);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
